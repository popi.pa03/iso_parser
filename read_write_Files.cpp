#include <sstream>

#include "read_write_Files.hpp"
#include "isoParser_Objects.hpp"


void printMessageVector(std::ofstream& stream, std::vector<std::string>& vec){

  stream <<"\t\t" ;

  for(unsigned int i = 0 ; i < vec.size(); ++i){
    stream << vec[i] << "";
  }
  stream << std::endl;


}

void printTags(std::ofstream& stream,  Field& f){

  for(unsigned int i = 0 ; i < f.getTags().size(); i++){
    stream  << "\t\t" 
      << f.getTags()[i].getName()  << " : "<< endl;
    
    for(unsigned int j=0; j < f.getTags()[i].getValue().size(); j++){
      stream << "\t\t\t" 
      << f.getTags()[i].getValue()[j] << " ";
    }
    stream << endl;
 
   }

}

void printFieldsToFile(std::ofstream& stream, std::vector<Field>& messageFields){

  for(unsigned int i = 0 ; i < messageFields.size(); ++i){
    stream 
        << "\t\t"  << messageFields[i].getFieldName() << ". " 
        // << messageFields[i].getType() << " " 
        // << messageFields[i].getLength() << " " 
        // << messageFields[i].getTypeOfLength() << " "
        << messageFields[i].getDescription() << ": " << endl;
    
    (messageFields[i].getTags().size() > 1) ? printTags(stream, messageFields[i]) : printMessageVector(stream, messageFields[i].getMessage());

    stream << std::endl;
  }

}
 
/*
Function that selects the vector for print. 
  Inputs: 
    Type of the field ["mti","bitmap","bitmap2"], 
    Transaction message
  if invalid type: returns an empty vector
*/
const auto selectVec(const std::string& type, TransactionMessage& message){

  if(type=="mti"){
    return message.getData().getMTI();
  }
  else if(type == "bitmap"){
    return message.getData().getBitmap();
  }
  else if(type == "bitmap2"){
    return message.getData().getBitmap2();     

  }
  else if(type == "header"){
    return message.getHeader().getMessage();
  }
  else if(type == "trailer"){
    return message.getTrailer().getMessage();
  }
  return std::vector<std::string>(0);

}

/*
  Function that prints the index of a vector.
  First, it selects which vector to print according to the given type.
  Then it prints.
  Inputs:
    stream of the file
    transaction message
    type of the field to be printed
*/
void printToFile(std::ofstream& stream, TransactionMessage& message, const std::string& type){

  stream << "      ";
  const auto vec = selectVec(type, message);
  for (const auto &i : vec)
   {
     stream <<  i ;
   }
   stream << std::endl;
}

 void printFields(std::ofstream& stream, TransactionMessage& message){

  for (const auto &i : message.getData().getField())
   {
     stream << i.getFieldName() << ": " << i.getType() << " " << i.getLength() << " |";

   }
}

/*
  Function that prints the results of the process to a new file.
  The name of the file is given by the user.
  Inputs:
    The name of the file to be created and written.
    The transaction message.
*/
void fileWrite(const std::string& outFilename, TransactionMessage& message, std::vector<std::string> messageVector){
  time_t ttime = time(0);
  char* dt = ctime(&ttime);

  std::ofstream output_File;
  
  output_File.open(outFilename);
  output_File << "PROTOCOL : ISO-8583" << std::endl;
  output_File << "READING TIMESTAMP: " << dt << std::endl;
  
  // output_File << "    MESSAGE: " << std::endl;
  // printMessageVector(output_File, messageVector);
  
  output_File << "    ::: The results are::: " << std::endl;
  
  if(message.getHeader().getMessage().size()){
    output_File << "    ___HEADER:___" << std::endl;
    printToFile(output_File, message, "header");
  }
  
  output_File << "    ___MTI:___" << std::endl;
  printToFile(output_File, message, "mti");

  output_File << "    ___Primary Bitmap:___"<< std::endl;
  printToFile(output_File, message, "bitmap");
  
  if(message.checkForSecondBitmap()){

    output_File << "    ___Secondary Bitmap:___" << std::endl;
    printToFile(output_File, message, "bitmap2");
  
  }
  
  output_File << "    ___Fields:___ "<< std::endl;
  printFieldsToFile(output_File, message.getData().getField());

  if(message.getTrailer().getMessage().size()){
    
    output_File << "    ___TRAILER:___" << std::endl;
    printToFile(output_File, message, "trailer");
  
  }
  

  output_File.close();
}

/* 
  NOTE: [Function overloading]
  Function that inserts one line of the hex message to a vector.
  It does not insert the spaces.
  Inputs: 
    specific line index
    the transaction message vector refrence, instead of return
*/
void insertToVector(std::string& line, std::vector<std::string>& syncMessage) {

  std::stringstream stream(line);
  std::string word;

  while(stream >> word){
    syncMessage.push_back(word);
  }


}

void insertToVector(std::string& line, std::vector<Field>& fieldVec) {


  std::stringstream stream(line);

  std::string out;
  std::string x;
  std::string len;
  std::string fname;
  std::string type;
  std::string description;

  stream >> fname;

  stream >> type;
 
  stream >> len;

  while(stream >> out){
    stringstream convert(out);
  
    convert >> x;
  
    description+=" "+x;
 
  }
  
  Field temp_f(fname, type, len, description);
  
  fieldVec.push_back(temp_f);

  
}

void insertToVector(std::string& line, std::vector<Tag>& tagVec) {

  std::stringstream stream(line);

  std::string name;
  std::string length = "0";
  std::string format;
  std::string required;

  std::vector<Tag> tagVec2(1, Tag());
  
  
  stream >> name;
  
  stream >> required;

  stream >> format;

  stream >> length;

  Tag t(name, length, format, required);

  tagVec.push_back(t);
}

void insertToHT(std::string& line, HashTable& tagHT){

  std::stringstream stream(line);

  std::string name;
  std::string length = "0";
  std::string format;
  std::string required;

  Tag tag;
  stream >> name;
  tag.setName(name);

  stream >> required;
  tag.setRequired(required);

  stream >> format;
  tag.setFormat(format);

  stream >> length;
  tag.setLength(length);


  tagHT.insertElement(tag, name);


}

/*
  NOTE: [Function overloading]
  Function that get the specific content of the given line and calls for vector insertion.
  Inputs:
    line of the text file
    the transaction message vector refrence, instead of return
*/
void getFileData (const std::string& line, std::vector<std::string>& syncMessage) {

  std::string str = line.substr(SYNC_MESSAGE_START, SYNC_MESSAGE_LENGTH_PER_LINE);
  insertToVector(str, syncMessage);

}

void getFileData (const std::string& line, std::vector<Field>& data) {

  std::string str = line;
  insertToVector(str, data);

}

void getFileData2 (const std::string& line, HashTable& ht) {

  std::string str = line;
  insertToHT(str, ht);
}

void tagfileRead(const std::string& inFilename,  HashTable& ht){
  
  std::ifstream input_File (inFilename);
  std::string line;

  if (input_File.is_open())
  {
    while ( getline (input_File,line) )
    {

      getFileData2(line, ht);

    }

    input_File.close();
  }

  else {std::cout << "Unable to open file" << std::endl; exit(-1);}
}