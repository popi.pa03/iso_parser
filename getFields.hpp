#ifndef GETFIELDS__HPP
#define GETFIELDS__HPP

#include <vector>
#include <string>
#include "isoParser_Objects.hpp"
#include "hash_table.hpp"

inline bool checkWithSTRINGPOS(size_t found);
std::string getStr(const std::vector<std::string>& vec);

std::vector<std::string> slices(const std::vector<std::string>& syncMessage, unsigned int from, unsigned int to);
int slice(const std::vector<std::string>& syncMessage, unsigned int from, unsigned int to);
vector<string> slices_reverse(const vector<string>& syncMessage, unsigned int from);

void fillFixedLengthField(unsigned int fieldLength, const std::vector<std::string>& asciiMessage, const std::vector<std::string>& vecHex, unsigned int& c, TransactionMessage& message);
void fillVarLengthField(Field& field, std::vector<std::string>& asciiMessage, std::vector<std::string>& vecHex, unsigned int& c, TransactionMessage& message);

void find_Field(const std::vector<std::string>& syncMessage, TransactionMessage& message, unsigned int m, const std::string& fieldName);
void findFieldsAndMap(const std::vector<std::string>& vecOfBCD, const std::vector<Field>& all_ISOFieldsVector, TransactionMessage& message);
void findWhichFieldsExist(TransactionMessage& message, const std::vector<Field>&  all_ISOFieldsVector);
void parseFields(const std::vector<std::string>& asciiMessage, const std::vector<std::string>& vecHex, TransactionMessage& message );

struct retVal{
  bool exists;
  std::string times;
};

retVal checkIfFixedLenField(const std::string& str);
void checkFieldTypeOfLength(std::vector<Field>&  messageFields);
void getSubFields(const vector<string>& messageVector, HashTable& ht, Field* f, TransactionMessage& message);

#endif
