#ifndef ISOPARSER_OBJECTS__HPP
#define ISOPARSER_OBJECTS__HPP

#include <vector>
#include <string>
#include <fstream>
#include <iostream>
#include <algorithm>

#include "converts.hpp"

#define MTI_BYTES 4

#define BITMAP_BYTES 16

#define SYNC_MESSAGE_START 0
#define ASCII_SYNC_MESSAGE_START 49

#define SYNC_MESSAGE_LENGTH_PER_LINE 47
#define ASCII_SYNC_MESSAGE_LENGTH_PER_LINE 17

/*
We assume and define that an ISO message consists of 3 major parts: the header, the application data, and the trailer
the header and the trailer are used for routing and message integrity.
The application data consists of MTI (Message Type Indicator), BITMAP, and the data Field of the message

Our implementation, uses the Field class, containing the fieldname, the type of the data, and the message
*/

class Header{

private:
  std::vector<std::string> message;
  unsigned int bytes;

public:

  Header(){
    this->bytes = 0;
    message = std::vector<std::string>(0);

    std::cout << "Header has just been created" << std::endl;
  }

  ~Header(){
    std::cout << "Header has just been destroyed" << std::endl;
  }

  unsigned int getBytes(){
    return this->bytes;
  }

  std::vector<std::string>& getMessage() {
    return this->message;
  }

  void setMessage(const std::vector<std::string>& message){
    this->message = message;
  }

  void setBytes(unsigned int bytes){
    this->bytes = bytes;
  }
};

class Trailer{
  private: 

  public:
  std::vector<std::string> message;// = std::vector<std::string>(0);
  unsigned int bytes;

  Trailer(){
    this->bytes = 0;
    this->message = std::vector<std::string>(0);

    std::cout << "Trailer has just been created" << std::endl;
  }

  ~Trailer(){
    std::cout << "Trailer has just been destroyed" << std::endl;
  }

  std::vector<std::string>& getMessage() {
    return this->message;
  }

  unsigned int getBytes(){
    return this->bytes;
  }

  void setMessage(const std::vector<std::string>& message){
    this->message = message;
  }

  void setBytes(unsigned int bytes){
    this->bytes = bytes;
  }
};

class Tag{
  
  std::string name;
  std::string length = "0";
  std::vector<std::string> value;
  std::string format;
  std::string required;

  public:
 
  Tag(){

    this->length="0";
    value = std::vector<std::string>(0);
  //  std::cout << "A tag is created!" <<std::endl;
  } 

  Tag(std::string& name, std::string& length, std::string& format, std::string& required){
    this->name = name;
    this->length = length;
    this->format = format;
    this->required = required;

  }
  ~Tag(){
  //  std::cout << "A tag has been destroyed!" <<std::endl;
  }

  void setName(std::string& name){
    this->name = name;
  }

  void setLength(const std::string& length){
    this->length = length; 
  }

  void setFormat(std::string& format){
    this->format = format; 
  }

  void setRequired(std::string& required){
    this->required =required;
  }

  void setValue(const std::vector<std::string>& value){
    this->value = value;
  }  

  std::string& getName(){
    return this->name;
  }
  std::string& getLength(){
    return this->length;
  }
  std::string& getFormat(){
    return this->format;
  }
  std::string& getRequired(){
    return this->required;
  }
  std::vector<std::string>& getValue(){
    return this->value;
  }

  /*
    This function is used to insert a tag into the hashtable
  */
  void getKey(unsigned& key){
    
    try{
        
      key = std::stoi(this->getName());
       
    }
    catch(std::exception const &e){
      std::cout << "Error: " << e.what() << std::endl;
    }
  }
  
};

class Field{

  std::string fieldName; //f1, f2...
  std::string type; //N, ans,....
  std::string length; //3 ... ;
  std::string typeOfLength; //'f': fixed, 'v': not fixed;
  std::string typeOfCounting; //'a': ascii, 'b': bcd, 'n': num
  std::string varIndicatorBytes; //for variable length 

  //Field 55 has multiple tags
  std::vector<Tag> tags;
  std::vector<std::string> message; //ascii codes
  std::vector<std::string> hexMessage; //hex codes

  std::string description;

  public:
  
  Field(){

    this->tags = std::vector<Tag>(0);
    this->message = std::vector<std::string>(0);
    this->hexMessage = std::vector<std::string>(0);
    //std::cout << "Field has just been created!!! " << std::endl;
  }

  Field(std::string& fieldName, std::string& type, std::string& length, std::string& description){
    this->fieldName = fieldName;
    this->type = type;
    this->length = length;
    this->description = description;
  }

  ~Field(){
    //std::cout << "Field has just been destroyed!!! " << std::endl;
  }

  void setFieldName(std::string& fieldName){
    this->fieldName =  fieldName;
  }

  void setType(std::string& type){
    this->type = type;
  }

  void setMessage(const std::vector<std::string>& message){
    this->message = message;
  }

  void setHexMessage(const std::vector<std::string>& message){
    this->hexMessage = message; 
  }

  void setLength(std::string& len){
    this->length = len;
  }

  void setTypeOfLength(const std::string& len){
    this->typeOfLength =len;
  }

 void setVarIndicatorBytes(std::string& bytes){
    this->varIndicatorBytes = bytes;
  }

  void setDescription(std::string& description){
    this->description = description;
  }

  void setTags(const std::vector<Tag>& tags){
   this->tags = tags;
  }

  std::string getFieldName() const{
    return this->fieldName;
  }

  std::string getType() const{
    return this->type;
  }

  std::vector<std::string>& getMessage() {
    return this->message;
  }

  std::string getLength() const{
    return this->length;
  }

  std::string getTypeOfLength() {
    return this->typeOfLength;
  }

  std::string getVarIndicatorBytes() {
    return this->varIndicatorBytes;
  }

  std::string getDescription(){
    return this->description;
  }

  std::vector<Tag>& getTags(){
    return this->tags;
  }

  std::vector<std::string>& getHexMessage(){
    return this->hexMessage;
  }
};

class ApplicationData{

  std::vector<std::string> mti;
  std::vector<std::string> bitmap;
  std::vector<std::string> bitmap2;
  std::vector<Field> field;

  public:

  ApplicationData(){
    
    mti = std::vector<std::string>(0);
    bitmap = std::vector<std::string>(0);
    bitmap2 = std::vector<std::string>(0);
    field = std::vector<Field>(0);
    
    std::cout << "ApplicationData has just been created!!! " << std::endl;
  }

  ~ApplicationData(){
    std::cout << "ApplicationData has just been destroyed!!! " << std::endl;
  }

  void setMTI(const std::vector<std::string>& mti){
    this->mti= mti;
  }

  void setBitmap(const std::vector<std::string>& bitmap){
    std::vector<std::string> vecOfAscii(0);
    hexToAscii(bitmap, vecOfAscii);

    std::vector<std::string> vecOfAsciiBCD(0);
    toBCD(vecOfAscii, vecOfAsciiBCD);

    this->bitmap= vecOfAscii;
    //this->bitmap= bitmap;
  }

  void setBitmap2(const std::vector<std::string>& bitmap2){
   
    std::vector<std::string> vecOfAscii(0);
    hexToAscii(bitmap2, vecOfAscii);

    std::vector<std::string> vecOfAsciiBCD(0);
    toBCD(vecOfAscii, vecOfAsciiBCD);
    
    this->bitmap2= vecOfAscii;  

    }

  void setField(const Field& field){
    this->field.push_back(field);
  }

  std::vector<std::string>& getMTI() {
    return this->mti;
  }
  std::vector<std::string>& getBitmap() {
    return this->bitmap;
  }

  std::vector<std::string>& getBitmap2() {
    return this->bitmap2;
  }

  std::vector<Field>& getField() {
    return this->field;
  }

  auto* getSpecificField(const std::string& name) {

    std::vector<Field>& vec =  this->getField();
    
    auto it = std::find_if(vec.begin(), vec.end(), [&name](const Field& field){return field.getFieldName() == name;});

    return it!= vec.end() ? &*it : NULL;
  }

};

class TransactionMessage{

  Header header;
  Trailer trailer;
  ApplicationData data;
  bool second_bitmap;
  bool is_header_here;
  bool is_trailer_here;
  unsigned int currentPosition;

  public:

  TransactionMessage(){
    this->currentPosition =0;
    std::cout << "Transaction message has just been created!!! " << std::endl;
  }

  ~TransactionMessage(){
    std::cout << "Transaction message has just been destroyed!! " << std::endl;
  }

  void setHeader(Header& header){
    this->header= header;
  }

  void setTrailer(Trailer& trailer){
    this->trailer= trailer;
  }

  void setData(ApplicationData& data){
    this->data = data;
  }

  void setIS_Second_Bitmap(bool second_bitmap){
    this->second_bitmap = second_bitmap;
  }

  void setIS_Header_Here(bool val){
    this->is_header_here = val;
  }

  void setIS_Trailer_Here(bool val){
    this->is_trailer_here = val;
  }

  void increaseCurrentPosition(unsigned int bytesToIncrease){
    this->currentPosition+= bytesToIncrease;

  }

  Header& getHeader(){
    return this->header;
  }

  Trailer& getTrailer(){
    return this->trailer;
  }

  bool getIS_Second_Bitmap(){
    return this->second_bitmap;
  }

  bool getIS_Header_Here(){
    return this->is_header_here;
  }

  bool getIS_Trailer_Here(){
    return this->is_trailer_here;
  }

  unsigned int getCurrentPosition(){
    return this->currentPosition;
  }

  ApplicationData& getData() {
    return this->data ;
  }

  /************ PRINTS ************/

  void printMTI(){
    
    for (const auto &i : this->getData().getMTI())
     {
       std::cout <<  i ;
     }
     std::cout << std::endl;
  }

  void printBitmap(){

    for (const auto &i : this->getData().getBitmap())
     {
       std::cout << i ;
     }
     std::cout << std::endl;
  }

   void printFields(){

    for (const auto &i : this->getData().getField())
     {
       std::cout << i.getFieldName() << ": " << i.getType() << " " << i.getLength() ;

     }
  }

  /************ Useful Functions ************/
  /*
  Function that checks if the  current transaction message has a second bitmap.
  Notes: 
    if the first byte of the bcd representation is not 0 : it has 2nd bitmap.
  */
  bool checkForSecondBitmap(){
    
    std::vector<std::string> vecOfBCD;
    toBCD(this->getData().getBitmap(), vecOfBCD);
    
    if(vecOfBCD.at(0)[0] != '0'){
  
      this->setIS_Second_Bitmap(true);
      return true;
      
    }

    return false;
    
  }

  void increaseAndSetNewCurrentPosition(unsigned int &c, unsigned int len){
    this->increaseCurrentPosition(len);
    c = this->getCurrentPosition();
  }

  void setMessageVectorsAndIncreasePosition(unsigned int& c, unsigned int fieldLength, unsigned int i, const std::vector<std::string>& vec, const std::vector<std::string>& vecHex){
    
    this->increaseAndSetNewCurrentPosition(c, fieldLength);

    this->getData().getField()[i].setMessage(vec);
    
    this->getData().getField()[i].setHexMessage(vecHex);
  }
    

  bool field55_InMessage(){
    
    std::string str= "55";
    std::vector<Field>& vec =  this->getData().getField();
    
    auto it = std::find_if(vec.begin(), vec.end(), [&str](const Field& field){return field.getFieldName() == str;});

    if(it != vec.end()){
      return true;
    }

    return false;
  }
};

#endif
