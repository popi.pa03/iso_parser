#include "isoParser_Objects.hpp"
#include "parameters.hpp"
#include "read_write_Files.hpp"
#include "getFields.hpp"
#include "hash_table.hpp"

#define TAG_HASH_BUCK_SIZE 6

using namespace std;


int main(int argc, char const *argv[]) {

  Params params(argc, argv);
  TransactionMessage transactionMessage;
  
  vector<Field> all_ISOFieldsVector (0, Field());
  
  vector<string> messageVector(0); //hex
  vector<string> vecOfAscii(0); //same message - ascii values
  
  // fill significant vectors
  fileRead<string>(params.getFileI(), messageVector);
  fileRead<Field>(params.getISO_Version(), all_ISOFieldsVector);
  hexToAscii(messageVector, vecOfAscii);

  /*filling the message's fields*/
    /* 1st: in filling the header */
  if(params.getHeaderIsHere()){

    unsigned int bytes = params.getHeaderBytes();
    transactionMessage.setIS_Header_Here(true);
    transactionMessage.getHeader().setBytes(bytes);
    
    find_Field(vecOfAscii, transactionMessage, bytes, "header");
    
    transactionMessage.increaseCurrentPosition(bytes);
    
  }

    /* Then: in checking for the mti*/
  find_Field(vecOfAscii, transactionMessage, transactionMessage.getCurrentPosition(),"mti");
  transactionMessage.increaseCurrentPosition(MTI_BYTES);

    /* Then: in filling the primary bitmap*/
  find_Field(messageVector, transactionMessage, transactionMessage.getCurrentPosition(), "bitmap");
  transactionMessage.increaseCurrentPosition(BITMAP_BYTES);
 
    /* Then: if there is a second bitmap, fill it too*/
  if(transactionMessage.checkForSecondBitmap()){
  
    find_Field(messageVector, transactionMessage, transactionMessage.getCurrentPosition(), "bitmap2");
    transactionMessage.increaseCurrentPosition(BITMAP_BYTES);
    
  }

  /* last but not least: in filling the trailer */
  if(params.getTrailerIsHere()){
    unsigned int bytes = params.getTrailerBytes();
    transactionMessage.setIS_Trailer_Here(true);
    transactionMessage.getTrailer().setBytes(bytes);
    
    find_Field(vecOfAscii, transactionMessage, bytes, "trailer");
    
  }

  //convert hex to BCD & find which fields from bitmap
  vector<string> vecOfBCD(0);
  toBCD(transactionMessage.getData().getBitmap(), vecOfBCD);

  findWhichFieldsExist(transactionMessage, all_ISOFieldsVector);
  

  //fixed or variable
  checkFieldTypeOfLength(transactionMessage.getData().getField());
  
  //3.fill message's Fields
  parseFields(vecOfAscii, messageVector, transactionMessage);
  
  //SubFields: 
  //Find the field you want
  Field* f= transactionMessage.getData().getSpecificField("55");

  if(f!= nullptr){
  
    HashTable all_Tags(TAG_HASH_BUCK_SIZE);
    
    tagfileRead(params.getTagFile(), all_Tags);

    getSubFields(f->getHexMessage(), all_Tags, f, transactionMessage);  


  }
    
  /***************************************************************/
  //print results
  fileWrite(params.getFileO(), transactionMessage, messageVector);

  return 0;
}
