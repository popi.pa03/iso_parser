#ifndef PARAMETERS__HPP
#define PARAMETERS__HPP

#include <string>
#include <cstring>
#include <iostream>
#include <fstream>
#include <sstream>


using namespace std;


/*
    This file contains the basic assignment of the filenames {input & output}
    There are two ways to submit the parameters:

      1. from cmd with the flags -i <input_File> -o <output_File> -h <header_bytes> -t <trailer_bytes>
      2. from a menu

    BE CAREFUL: 
      In any case, we should enter names both for input and output Files!
      Otherwise, the program will never continue...
      The Header and the trailer are optional. 

    NOTE: 
      If the inputFile does not exist, the program waits until a existent filename is given.
      The outputFiles are created by the application.
      In the case of further info menu about the header and the trailer bytes: If characters are given as input, we don't proceed in the assignment. 
*/

class Params{

  string filename_input = "";
  string filename_output = "";

  string iso_Version = "./FILES/iso_data_elements_1987.txt";
  string tag_File = "./FILES/F55_tags.txt";
  string params_File = "./FILES/params.txt";

  bool header_is_here = false;
  bool bitmap2_is_here = false;
  bool trailer_is_here = false;
  bool version_changed = false;

  unsigned int header_bytes = 0;
  unsigned int trailer_bytes = 0;
  
  public:

  Params(int argc, char const *argv[]) {
    vector<string> allArgs(argv, argv+argc);
    (argc == 1) ?
      this->read_Params_FromFile() : this->read_from_CMD(unsigned(argc), argv);
  
    this->read_FromMenu(); /*if a significant parameter is missing*/
  
    if(!this->getVersionChanged())
      this->doYouWantToChange_ISOversion();

    if(!this->getHeaderIsHere())
      this->requestForHeader_Trailer_Len("header");
    
    if(!this->getTrailerIsHere())
      this->requestForHeader_Trailer_Len("trailer");

  }


  void setFileI(const string& filename){
    this->filename_input = filename;
  }

  void setFileO(const string& filename){
    this->filename_output = filename;
  }

  void setHeader_is_Here(bool val){
    this->header_is_here = val;
  }

  void setBitmap2_is_Here(bool val){
    this->bitmap2_is_here = val;
  }

  void setTrailer_is_Here(bool val){
    this->trailer_is_here = val;
  }

  void setHeaderBytes(unsigned int bytes){
    this->header_bytes = bytes;
  }

  void setTrailerBytes(unsigned int bytes){
    this->trailer_bytes = bytes;
  }

  void setISO_Version(const string& version){
    this->iso_Version = version;
  }

  void setVersionChanged(bool changed){
    this->version_changed = changed;
  }

  void setTagFile(string& filename){
    this->tag_File = filename;
  }

  void setParamsFile(string& filename){
    this->params_File = filename;
  }

  string getFileI(){
    return this->filename_input;
  }

  string getFileO(){
    return this->filename_output;
  }

  bool getHeaderIsHere(){
    return this->header_is_here;
  }
  bool getBitmap2IsHere(){
    return this->bitmap2_is_here;
  }
  bool getTrailerIsHere(){
    return this->trailer_is_here;
  }

  unsigned int getHeaderBytes(){
    return this->header_bytes;
  }

  unsigned int getTrailerBytes(){
    return this->trailer_bytes;
  }

  string getISO_Version(){
    return this->iso_Version;
  }

  bool getVersionChanged(){
    return this->version_changed;
  }

  string getTagFile(){
    return this->tag_File;
  }

  string getParamsFiles(){
    return this->params_File;
  }

  /*
    Function that checks if the given file exists.
    Inputs:
      filetype: input or output file?
      filename: the given file name
  */
  bool checkValidation(const string& filetype, const string& filename){
    ifstream input_File (filename);

    if(!filename.length()) return false;

    if(filetype == "input"){

      return input_File.is_open();
    }

    return true;
  }

  /*
    Function that reads cmd line arguments
    FLAGS:
      -i: for input file
      -o: for output file
      -h: for the bytes of the header
      -t: for the bytes of the trailer
      -vf: for the version of the iso (default: 1987)
  */
  void read_from_CMD(unsigned int argc, const char* argv[]){

    for(unsigned int i = 1 ; i + 1 < argc ; i+=2){

      if( !strcmp(argv[i],"-i" )){
        (checkValidation("input", argv[i+1]))? this->setFileI(argv[i+1]) : waitForProperEntry("input");
      }

      else if(!strcmp(argv[i],"-o" )){
        this->setFileO(argv[i+1]);
      }

      else if(!strcmp(argv[i],"-h" )){
        this->setHeader_is_Here(true);
        this->setHeaderBytes(atoi(argv[i+1]));
      }

      else if(!strcmp(argv[i],"-t" )){
        this->setTrailer_is_Here(true);
        this->setTrailerBytes(atoi(argv[i+1]));
      }

      else if(!strcmp(argv[i],"-vf" )){
        this->setISO_Version(argv[i+1]);
        this->setVersionChanged(true);
      }

    }
  }

  /*
    Input & output file names must be submitted. 
    This function waits until a proper filename is given (name length > 0)
    For the input file also waits for a file that exists. 
  */
  void waitForProperEntry(const string& filetype){

    string filename = (filetype == "input") ? this->getFileI(): this->getFileO();

    do{

      cout << "Please insert a file name! For: " << filetype << endl;
      
      cin.clear();
      cin >> filename;
      
    }while(!checkValidation(filetype, filename));

    (filetype == "input") ? this->setFileI(filename): this->setFileO(filename);

    cout << "Thank you! " <<endl;
  }

  /*
    Function that gives the opportunity for the user to change the iso version
    by changing the iso file.
  */
  void doYouWantToChange_ISOversion(){

    string answer;

    do{

      cout << "By default the iso version is: ISO_" << this->getISO_Version()
      << ", and gets its data from the File \"./FILES/iso_data_elements_1987.txt\"" << endl
      << "Do you want to change it? [yes/no]" << endl;

      cin.clear();
      cin >> answer;
    
    }while(!(answer =="yes" || answer =="no"));
   
    if(answer == "yes"){
      string version;

      do{

        cout 
        << "Please insert the version filename! "<< endl
        << "Make sure that the file exists!" <<endl;
        
        cin.clear();
        cin >> version;
      
      }while(!checkValidation("version", version));
    }
    
  }

  /*
    This function is called for reading from the menu if cmd line args are not given
  */
  void read_FromMenu(){
    string filenameI = this->getFileI();
    string filenameO = this->getFileO();

    if(filenameI.length() == 0 ){
      waitForProperEntry("input");
    }

    if(filenameO.length() == 0 ){
      waitForProperEntry("output");
    }
  }

  void read_Params_FromFile(){
    std::ifstream input_File (this->getParamsFiles());
    std::string line, input, s;
    
    if (input_File.is_open()) {
      while ( getline (input_File,line) ) {
        std::stringstream stream(line);
         stream >> s;
         if( s == "-i" ){
           stream >> input;
          (checkValidation("input", input))? this->setFileI(input) : waitForProperEntry("input");
        }

      else if(s =="-o" ){
        stream >> input;
        this->setFileO(input);
      }

      else if(s == "-h" ){
        stream >> input;
        this->setHeader_is_Here(true);
        this->setHeaderBytes(stoi(input));
      }

      else if(s =="-t" ){
        stream >> input;
        this->setTrailer_is_Here(true);
        this->setTrailerBytes(stoi(input));
      }

      else if(s == "-vf" ){
        stream >> input;
        this->setISO_Version(input);
        this->setVersionChanged(true);
      }

      }
      
      input_File.close();
    }
    else std::cout << "Unable to open file" << std::endl;

  }

  /*
    This function checks if the given string consists of digits.
    In case of a character detection, a message is displayed 
  */
  bool allDigits(string type){
    for(unsigned int i= 0; i < type.size(); ++i){
      
      if(isalpha(type[i])) {
       
        cout << "Wrong value: Not assigned." << endl;
        return false;
        
      }
      
    }
 
    return true;
  }

  /*
    This function requests further info from the user, regarding parts like the header and the trailer.
  */
  string getbytes(const string& part){

    string type;
    string answer;

      cout << "Is there a " << part <<"? For a positive answer, please type 'yes'." <<endl;
      cin.clear();
      cin >> answer;

      if(answer == "yes"){
        cout << "Please type the number of " << part <<"'s bytes." << endl;
        cin.clear();
        cin >> type;
      }
    
    return type;
  }
  
  /*
    This function asks for further info about the header and the trailer of the message.
  */
  void requestForHeader_Trailer_Len(const string& type){
   
    cout << "Please provide further information" << endl;
  
    if(type == "header"){
   
      string header = getbytes("header");
      
      if (header.length() && allDigits(header)){
        this->setHeader_is_Here(true);
        this->setHeaderBytes(stoi(header));
      }  
      
    }
    if(type == "trailer"){
    
      string trailer = getbytes("trailer");
         
      if (trailer.length() && allDigits(trailer)){
        this->setTrailer_is_Here(true);
        this->setTrailerBytes(stoi(trailer));
      } 
    }
    
  }


};

#endif
