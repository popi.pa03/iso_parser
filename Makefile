CC = g++
CFLAGS = -Wall -g

main: isoParser.o
		$(CC) $(CFLAGS) -o exe isoParser.o read_write_Files.o getFields.o converts.o

isoParser.o: isoParser.cpp isoParser_Objects.hpp parameters.hpp converts.o read_write_Files.o getFields.o
		$(CC) $(CFLAGS) -c isoParser.cpp

read_write_Files.o: read_write_Files.hpp isoParser_Objects.hpp
		$(CC) $(CFLAGS) -c read_write_Files.cpp

getFields.o: getFields.hpp isoParser_Objects.hpp hash_table.hpp

converts.o: converts.hpp

clean:
		rm -rf *.o
