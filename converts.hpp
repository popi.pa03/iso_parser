#ifndef CONVERTS__HPP
#define CONVERTS__HPP

#include <vector>
#include <string>

void asciiToHex(const std::vector<std::string>& vecOfHex, std::vector<std::string>& vecOfAscii);
int hexToDecimal(const std::string& hex);
void hexToAscii(const std::vector<std::string>& vecOfHex, std::vector<std::string>& vecOfAscii);
void toBCD(const std::vector<std::string>& vecOfChars, std::vector<std::string>& vecOfBCD);

#endif