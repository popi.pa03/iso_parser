#include "getFields.hpp"
#include <sstream>

using namespace std;

/* This function is used for str find */
inline bool checkWithSTRINGPOS(size_t found){

  return (found != string::npos);
}

/* 
  Function that given a vector of strings, returns a string
*/
string getStr(const vector<string>& vec){
  
  string str = "";
  
  for(unsigned int i = 0; i < vec.size(); ++i){
    str+=vec[i];
  }

  return str;
}

/*
  Function that checks if the current string contains one, two, three dots.
  The number of dots reflects the L/LL/LLLVAR.
  The output is a struct which contains 1. if the dot exists. 2. how many times we have found the dot.
*/
retVal checkIfFixedLenField(const string& str){
 
  size_t f1 =str.find(".");
  size_t f2 =str.find("..");
  size_t f3 =str.find("...");

  bool v1 = checkWithSTRINGPOS(f1);
  bool v2 = checkWithSTRINGPOS(f2);
  bool v3 = checkWithSTRINGPOS(f3);

  retVal toReturn;
  toReturn.exists = false;
  toReturn.times = "0";

  /*str.contains works in c++23! */
  //if(str.contains(".")|| str.contains("..") || str.contains("...")){
  /* 
    str.find(" "): if no matches are found, it returns string::pos    
    Problem with find: does not recognize str2 with no spaces
  */
  if(v1  || v2  || v3 ){
 
    toReturn.exists = true;
    if(v1) toReturn.times = to_string(1);
    if(v2) toReturn.times = to_string(2);
    if(v3) toReturn.times = to_string(3);
    
  }

  return toReturn; 
   
}

/*
  Function that checks the type of field lenght given a vector of message's fields.
  For a fixed field, it sets the fixed type.
  For a variable field, it set the variable type and the indicatorbytes. 
  This number is according to the dots found
*/
void checkFieldTypeOfLength(vector<Field>&  messageFields){

  for(unsigned int i = 0; i < messageFields.size(); ++i){
  
    string type;
    retVal rV = checkIfFixedLenField(messageFields[i].getLength());

    if(!rV.exists){

      string type = "fixed";
      messageFields[i].setTypeOfLength(type);
    
    }
    else{
      string type = "variable";
      string lenIndicatorBytes = rV.times;

      messageFields[i].setTypeOfLength(type);
      messageFields[i].setVarIndicatorBytes(lenIndicatorBytes); //1,2, 3 analoga me tis '.'
    
    }
  }
 
}

/*
  Function that slices a given vector of strings given the starting and ending positions.
  This function returns the new vector.
  Input:
    syncMessage: initial vector to be sliced
    from: starting point to slice
    to: ending point
*/
vector<string> slices(const vector<string>& syncMessage, unsigned int from, unsigned int to){

  vector<string> vec {syncMessage.begin()+from, syncMessage.begin() + to};

  return vec;
}

/*
  Function that slices a given vector of strings given the starting and ending positions.
  This function returns an integer for variable length fields.
  Input:
    syncMessage: initial vector to be sliced
    from: starting point to slice
    to: ending point
*/
int slice(const vector<string>& syncMessage, unsigned int from, unsigned int to){

  vector<string> vec = slices(syncMessage, from ,to); //{syncMessage.begin()+from, syncMessage.begin() + to};

  try{

    return stoi(getStr(vec));
  }
  catch(exception const &e){

    cout << "Error: " << e.what() << endl;
    return 0;
  }
  

}

/*
  Function that slices a given vector of strings given the starting and ending positions.
  It slices the vector from the ending point to the bytes of the trailer.
  
  This function is used for filling the trailer's message
  Input:
    syncMessage: initial vector to be sliced
    from: trailer's bytes
*/
vector<string> slices_reverse(const vector<string>& syncMessage, unsigned int from){

  vector<string> vec {syncMessage.end() - from, syncMessage.end()};

  return vec;
}

/*
  Function that reads a vector of the message's BCD representation and maps to a vector of all the iso fields, read by the input file
  In this function, the message's fields are set. (But there is no set of the field's message)
*/
void findFieldsAndMap(const vector<string>& vecOfBCD, const vector<Field>& all_ISOFieldsVector, TransactionMessage& message){
  
  int count = 0;

  for(unsigned int i=0; i < vecOfBCD.size(); ++i){
  
    for(unsigned int j=0; j < 4; ++j){

      if(vecOfBCD[i][j] == '1') {
      
        message.getData().setField(all_ISOFieldsVector[count]);
          
      }
      
      count++;
    }
    
  }
}

/*
  Function that finds the fields of the message.
  Input: 
    transaction message
    all_ISOFieldsVector: is a vector of all the field values which are read by the given file
*/
void findWhichFieldsExist(TransactionMessage& message, const vector<Field>&  all_ISOFieldsVector){
    
  //message's bitmap is in Ascii
  vector<string> bitmap = message.getData().getBitmap();
  //message's bitmap is in BCD
  vector<string> vecOfBCD(0);
 
  if(message.getIS_Second_Bitmap()){
    vector<string> bitmap2 = message.getData().getBitmap2();
    //concat the second bitmap to the first one
    /* NOTE: it can be a tetriary bitmap - but it is not covered here */
    bitmap.insert(bitmap.end(), bitmap2.begin(), bitmap2.end());
  }

  toBCD(bitmap, vecOfBCD);

  findFieldsAndMap(vecOfBCD, all_ISOFieldsVector, message);

}

/*
  Function that selects the field to slice and sets to some of the fields to the message.
  Input: 
    syncMessage: vector of hex values read by the input file
    transaction message: to be filled
    m: position to start the slice
    fieldName: is for the proper select
*/
void find_Field(const vector<string>& syncMessage, TransactionMessage& message, unsigned int m, const string& fieldName){

  unsigned int c = message.getCurrentPosition();
  if(fieldName == "mti"){
    
    vector<string> vec = slices(syncMessage, c, c+MTI_BYTES);
    message.getData().setMTI(vec);

  }
  else if(fieldName == "bitmap"){
    
    vector<string> vec = slices(syncMessage, c, c+BITMAP_BYTES);
    message.getData().setBitmap(vec);
  
  }
  else if(fieldName == "bitmap2"){
  
    vector<string> vec = slices(syncMessage, c, c+BITMAP_BYTES);
    message.getData().setBitmap2(vec);
  
  }
  else if(fieldName == "header"){

    vector<string> vec = slices(syncMessage, 0, m);
    Header& header = message.getHeader();
    
    header.setBytes(m);
    header.setMessage(vec);

    message.setHeader(header);
  }
  else if(fieldName == "trailer"){

    vector<string> vec = slices_reverse(syncMessage, m);
    Trailer& trailer = message.getTrailer();
    
    trailer.setBytes(m);
    trailer.setMessage(vec);

    message.setTrailer(trailer);
  }

}

/*
  Function that fills a fixed length field
*/
void fillFixedLengthField(unsigned int fieldLength, const vector<string>& asciiMessage, const vector<string>& vecHex, unsigned int& c, TransactionMessage& message, unsigned int i){
  
  vector<string> vec = slices(asciiMessage, c, c+ fieldLength);
  vector<string> vecHex2 = slices(vecHex, c, c+ fieldLength);

  message.setMessageVectorsAndIncreasePosition(c, fieldLength, i, vec, vecHex);

}

/*
  Function that fills a var length field
*/
void fillVarLengthField(unsigned int indicatorBytes, const vector<string>& asciiMessage, const vector<string>& vecHex, unsigned int& c, TransactionMessage& message, unsigned int i){
  
  //take L, LL, LLL length
  unsigned int length = slice(asciiMessage, c, c+ indicatorBytes);
  message.increaseAndSetNewCurrentPosition(c, indicatorBytes);

  vector<string> vec = slices(asciiMessage, c, c+ length);
  vector<string> vecHex2 = slices(vecHex, c, c+ length);

  message.setMessageVectorsAndIncreasePosition(c, length, i, vec, vecHex);
  
}

/*
  Function that takes the fields according to the field type
*/
void parseFields(const vector<string>& asciiMessage, const vector<string>& vecHex, TransactionMessage& message){
 
  unsigned int c = message.getCurrentPosition();

  vector<Field> fields = message.getData().getField();
 
  for(unsigned int i = 0; i < fields.size(); ++i){
    
    if(fields[i].getTypeOfLength() == "fixed"){
      
      try{
      
        unsigned int len = stoi(fields[i].getLength());
        fillFixedLengthField(len, asciiMessage, vecHex, c, message, i);
      
      }
      catch(exception const &e){
        cout << "Error: " << e.what() << endl;
      }
      
    
    }
      
    if(fields[i].getTypeOfLength() == "variable"){
      
      try{
    
      unsigned int lenInd = stoi(fields[i].getVarIndicatorBytes());
      fillVarLengthField(lenInd, asciiMessage, vecHex, c, message, i);
      
      }
      catch(exception const &e){
        cout << "Error: " << e.what() << endl;
      }
   
    }
  }
 
}

/*
 This function is for field 55 or similar fields that have many subfields.
 It also sets the tags of a given field
 Input:
  messageVector: Vector of message's HEX values
  ht: The hashtable of tag values
  field: The field that has the tags
*/
void getSubFields(const vector<string>& messageVector, HashTable& ht, Field* f, TransactionMessage& message){
 
  bool isTag = false;

  for(unsigned int position =0 ; position < messageVector.size();){
    
    retVals rVs = ht.findElem(messageVector[position]);

    if(!rVs.exists){ 

      string str2 = messageVector[position]+messageVector[position+1];
      rVs = ht.findElem(str2);

      if(!ht.findElem(str2).exists){

        isTag = false;
        position++;
      }
      else{
        
        isTag = true;
        position+=2;
      }
  
    }
    else{
            
      isTag = true;
      position++;
    }
    
    if(isTag){
   
      try{
        int len = stoi(messageVector[position]);    
      
        position++;
        vector<string> vec(0);

        for(unsigned int i =position; i < position+len; i++){
          vec.push_back(messageVector[i]);
        }

        position+=len;
 
        Tag *t = rVs.tag;
      
        if(t!=nullptr){

          t->setLength(to_string(len));
          t->setValue(vec);
          f->getTags().push_back(*t);
        }
      }
      catch(exception const &e){

        cout << "Error: " << e.what() << endl;
      }
     
          
    }
    message.getData().getSpecificField(f->getFieldName())->setTags(f->getTags());
  

  }

}
