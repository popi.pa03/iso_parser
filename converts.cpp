#include <bits/stdc++.h>
#include <sstream>

#include "converts.hpp"

using namespace std;

/*
    This file contains some useful functions, so as to convert a given vector 
    for estimating the field length and finding if there is a secondary bitmap

*/

void asciiToHex(const vector<string>& vecOfAscii, vector<string>& vecOfHex){
    stringstream sstream;
    for(unsigned int i = 0; i < vecOfAscii.size(); i++ ){
        
        sstream << std::hex << (vecOfAscii[i].c_str() - 0);
        
        string result = sstream.str();
        cout << result;
        vecOfHex.push_back(result);        
        
    }
}


/*  WARNING: NOT TESTED YET!
    Function that converts a string of hex values to a decimal number
*/
int hexToDecimal(const string& hex){

    int hexLen = hex.length();
    double dec = 0 ; //
    for(unsigned int i =0; i < hexLen; i++){

        char b = hex[i];
        if(b >=48 && b <=57){
            b-=48;
        }
        else if( b >=65 && b <=70){
            b-=55;
        }

        dec+= b*pow(16, ((hexLen -i)-1));

    }

    return (int) dec;
}

/*
    Function that converts a vector of hex to a vector of ascii code
    Inputs: 
        vector of hex values
        vector of ascii values to be filled during the process
*/
void hexToAscii(const vector<string>& vecOfHex, vector<string>& vecOfAscii){
  
    string ascii = "";
   
    for(size_t i = 0 ; i < vecOfHex.size(); i++){
   
        char ch = char(hexToDecimal(vecOfHex.at(i).substr(0,2)));
        string s(1,ch);
        vecOfAscii.push_back(s);
   
    }

}

/*
    Function that converts a vector of hex to a vector of bcd code
    Inputs: 
        vector of hex values
        vector of bcd values to be filled during the process
*/
void toBCD(const vector<string>& vecOfChars, vector<string>& vecOfBCD){

   for(unsigned int i =0; i < vecOfChars.size(); i++){

       string str = vecOfChars[i];

       for(unsigned int j = 0; j < str.length();j++){
     
            if(isdigit(str[j])){
                auto x = bitset<4>(str[j]);
                string s = x.to_string();
                vecOfBCD.push_back(s);
                
            }
            else{
                auto x = bitset<4>(str[j] -55);
                string s = x.to_string();
        
                vecOfBCD.push_back(s);
           
            } 
       }
   
    }

}

