#ifndef READ_WRITE_FILES__HPP
#define READ_WRITE_FILES__HPP

#include <fstream>
#include <string>
#include <iostream>
#include "hash_table.hpp"

#include "isoParser_Objects.hpp"


const auto selectVec(const std::string& type, TransactionMessage& message);
void printMessageVector(std::ofstream& stream, std::vector<std::string>& vec);
void printFieldsToFile(std::ofstream& stream, std::vector<Field>& messageFields);

//void fileWrite(const std::string& outFilename, TransactionMessage& message);
void fileWrite(const std::string& outFilename, TransactionMessage& message, std::vector<std::string> messageVector);

void printToFile(std::ofstream& stream, TransactionMessage& message, const std::string& type);
void printSyncMessage(std::vector<std::string> const &syncMessage, std::vector<std::string>::iterator mViterator);

void insertToVector(std::string& line, std::vector<Tag>& fieldVec) ;
void insertToVector(std::string& line, std::vector<Field>& fieldVec) ;
void insertToVector(std::string& line, std::vector<std::string>& syncMessage);
void insertToHT(std::string& line, HashTable& tagHT);

void getFileData (const std::string& line, std::vector<std::string>& syncMessage);
void getFileData (const std::string& line, std::vector<Field>& syncMessage);
void getFileData (const std::string& line, HashTable& syncMessage);

/*
  Function that reads the file line by line and calls for the vector insertion.
  Inputs:
    The name of the file
    A generic vector refrence, instead of return
      This vector might be a vector of strings : reading the input file
                        or a vector of Fields objects: reading the data field file
*/
template <typename T>
void fileRead(const std::string& inFilename,  std::vector<T>& vec){
  
  std::ifstream input_File (inFilename);
  std::string line;

  if (input_File.is_open())
  {
    while ( getline (input_File,line) )
    {

      getFileData(line, vec);

    }

    input_File.close();
  }

  else {std::cout << "Unable to open file" << std::endl; exit(-1);}
}

void tagfileRead(const std::string& inFilename,  HashTable& ht);


#endif
