#ifndef HASH_TABLE__HPP
#define HASH_TABLE__HPP

#include <cstring>
#include <iostream>
#include <map>
#include "isoParser_Objects.hpp"

#include "converts.hpp"
#include <vector>


using namespace std;

struct retVals{
        bool exists;
        Tag* tag;
    };

class HashTable{
    private: 
    
    vector<vector<Tag>> table;
    int total;
    
    unsigned getHash(string& str) {
        
        map<string, int> m;
        
        try{
            string sstr = str.substr(0,1);
            int key = stoi(sstr);
            m[str]  = key %total ;
        }
        catch(exception const &e){
            cout << "Error: " << e.what() << endl;
            return 0;
        }  
        

        return m[str];
    }

    public:

    HashTable(int n): table(n), total(n){}

    void insertElement(Tag tag, string key){
        table[getHash(key)].emplace_back(move(tag));
    }

    auto getElement(string key){
        
        vector<Tag>::iterator i;

        vector<Tag>& v = getBucket(key);

        for(i = v.begin(); i!= v.end(); i++){
            if(i->getName() == key)
                return *i;
        }

    }

    vector<Tag>& getBucket(string key){
        return table[getHash(key)];
    }
    
    retVals findElem(string key){
 
        retVals rVs;
         
        int x = getHash(key);
        vector<Tag>::iterator i;

        for(i = table[x].begin(); i!=table[x].end(); ++i){
            if(i->getName() == key){
                break;
            }
           
        }

        if(i!=table[x].end()){

            rVs.exists = true;
            rVs.tag = &(*i);
            return rVs;
        }

        rVs.exists =false;
        rVs.tag = nullptr;
        return rVs;
        
    }

    bool elemExists(string key){
        
        int x = getHash(key);
        vector<Tag>::iterator i;

        for(i = table[x].begin(); i!=table[x].end(); ++i){
            if(i->getName() == key){
                break;
            }
           
        }

        if(i!=table[x].end()){
            return true;
        }

        return false;
        
    }

    void printVec(vector<Tag>& t){
        for(auto i: t)
            cout << i.getName() << " ";
    }

    void printHT(){
        for(unsigned int i = 0; i < this->table.size(); i++){
          printVec(this->table[i]);
          cout << endl;
        }

    }

};

#endif